const config = require('./config.json')['initialNVAObjects'];
const base = require('./base.json')['initialNVAObjects'];

// create object of similar object.id in config.json and base.json
// key   === index element in base.json
// value === index element in config.json
const createSimillarObj = (config, base) => {
    const allObjWithSameId = {}
    config.forEach((elemConfig, indexConfig) => {
        // can not use Array.find beacause I need ALL elemnts
        base.forEach((elementBase, index) => {
            if(elemConfig.id === elementBase.id)
                allObjWithSameId[index] = indexConfig
        });
    })
    return allObjWithSameId;
}

const finalArr = (config, base) => {
    if(!base) {
        return config
    }
    const simillarObj = createSimillarObj(config, base)
    const arrToReturn = [];
    // arr of index config.json that we should not to push in arrToReturn
    const indexArrToDiscard = [];
    base.forEach((elemBase, index) => {
        // push update value if need
        if(Object.keys(simillarObj).includes(index.toString())) {
            indexArrToDiscard.push(simillarObj[index]);
            // push updaye value base.json --> config.json
            arrToReturn.push(config[simillarObj[index]])
        } else {
            arrToReturn.push(elemBase)
        }
            
    })
    // push other value from config.json
    config.forEach((elem, index) => {
        if(!indexArrToDiscard.includes(index))  
            arrToReturn.push(elem)
    })
    return arrToReturn
}

const res = finalArr(config, base)
console.log(res)